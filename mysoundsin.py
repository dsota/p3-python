from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        """
        Initializes a new instance of the class with the specified duration.

        @param duration: The duration of the sound in seconds
        @param frequency: The frequency of the sine wave in Hz
        @param amplitude: The amplitude of the sine wave
        @returns: None
        """
        super().__init__(duration)
        self.sin(frequency, amplitude)
