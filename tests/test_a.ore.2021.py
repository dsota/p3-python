from mysound import Sound
from soundops import soundadd
import unittest


class TestSoundops(unittest.TestCase):
    def test_diferencia_s1(self):
        s1 = Sound(1)
        s2 = Sound(2)
        s1.sin(440, 10)
        s2.sin(220, 50)
        l1 = soundadd(s1, s2)
        self.assertGreater(s2.duration, s1.duration)
        self.assertGreater(s2.nsamples, s1.nsamples)
        self.assertNotEqual(len(s1.buffer), len(s2.buffer))
        self.assertEqual(l1.duration, s2.duration)

    def test_valores(self):
        s1 = Sound(1)
        s2 = Sound(2)
        s1.sin(440, 70)
        s2.sin(220, 30)
        l3 = soundadd(s1, s2)
        s3 = Sound(2)
        s4 = Sound(1)
        s3.sin(220, 30)
        s4.sin(440, 70)
        l4 = soundadd(s3, s4)
        self.assertEqual(s3.nsamples, s2.nsamples)
        self.assertEqual(len(l3.buffer), len(l4.buffer))


if __name__ == '__main__':
    unittest.main()
