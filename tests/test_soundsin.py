import unittest
from mysound import Sound
from mysoundsin import SoundSin


class SoundSinTest(unittest.TestCase):

    def test_inherited_sound_properties(self):
        sound1 = SoundSin(1, 440, 10000)
        sound2 = Sound(1)
        sound2.sin(440, 10000)
        self.assertEqual(sound2.duration, sound1.duration)
        self.assertTrue(len(sound1.buffer), len(sound2.buffer))
        self.assertTrue(sound1.nsamples, sound2.nsamples)

    def test_sinusoid_functionalities_works_properly (self):
        sound1 = SoundSin(2, 440, 10000)
        self.assertTrue(all(-SoundSin.max_amplitude <= x <= SoundSin.max_amplitude
                            for x in sound1.buffer))
        self.assertEqual(0, sound1.buffer[0])
        self.assertEqual(0, sound1.buffer[22050])
        self.assertEqual(0, sound1.buffer[44100])

        sound2 = Sound(2)
        sound2.sin(440, 10000)
        self.assertEqual(sound2.buffer, sound1.buffer)
        self.assertEqual(sound2.buffer[0], sound1.buffer[0])
        self.assertEqual(sound2.buffer[0], sound1.buffer[22050])
        self.assertEqual(sound2.buffer[0], sound1.buffer[44100])

        period = 44100 / 440
        self.assertAlmostEqual(sound2.buffer[int(period * 0.25)],
                               sound1.buffer[int(period * 0.25)],
                               delta=2)
        self.assertAlmostEqual(sound2.buffer[int((period) * 1.25)],
                               sound1.buffer[int((period) * 1.25)],
                               delta=2)
    def test_sin_sound_bars_works_properly(self):
        sound1 = SoundSin(0.01, 440, 10000)
        bars = sound1.bars(bar_period=0.0001)

        self.assertAlmostEqual(112, len(bars.split('\n')), delta=1)
        self.assertAlmostEqual(41, len(bars.split('\n')[0]), delta=1)

        self.assertEqual(' ' * 40 + ':', bars.split('\n')[0])
        self.assertEqual(' ' * 40 + ':' + '*' * 12, bars.split('\n')[1])
        self.assertEqual(' ' * 40 + ':' + '*' * 20, bars.split('\n')[2])
        self.assertEqual(' ' * 40 + ':' + '*' * 28, bars.split('\n')[3])
        self.assertEqual(' ' * 40 + ':' + '*' * 32, bars.split('\n')[4])
        self.assertEqual(' ' * 40 + ':' + '*' * 36, bars.split('\n')[5])

        self.assertEqual(' ' * 33, bars.split('\n')[9][:33])

if __name__ == '__main__':
    unittest.main()
