import unittest
import mysound

class PrivateClassTests(unittest.TestCase):
    def test_bar(self):
        self.assertEqual((' ' * 40) + ':', mysound.Sound._bar(0))
        self.assertEqual((' ' * 40) + ':' + ('*' * 12), mysound.Sound._bar(12*250))
        self.assertEqual((' ' * 28) + ('*' * 12) + ':', mysound.Sound._bar(-12*250))


if __name__ == '__main__':
    unittest.main()
