from mysound import Sound
from soundops import soundadd
import unittest
import doctest


class TestSoundAdd(unittest.TestCase):
    def test_Sound_durations(self):
        sound1 = Sound(1)
        sound2 = Sound(2)
        sound3 = soundadd(sound1, sound2)
        """duration should be the greater of the two sounds
        >>> print(sound1.duration)
        1
        >>> print(sound2.duration)
        2
        >>> print(sound3.duration)
        2
        """
        self.assertEqual(sound3.duration, sound2.duration)
        self.assertTrue(sound3.duration > sound1.duration)

    def test_correct_sum(self):
        sound1 = Sound(2)
        sound2 = Sound(3)
        sound1.sin(440, 100)
        sound2.sin(440, 100)

        sound3 = soundadd(sound1, sound2)
        self.assertEqual(sum(sound3.buffer), sum(sound1.buffer) + sum(sound2.buffer))

    def test_conmutative(self):
        sound1 = Sound(2)
        sound2 = Sound(3)
        sound1.sin(440, 100)
        sound2.sin(440, 100)

        sound3 = soundadd(sound1, sound2)
        sound4 = soundadd(sound2, sound1)

        self.assertEqual(sound3.nsamples, sound4.nsamples)
        self.assertEqual(sound3.buffer, sound4.buffer)


if __name__ == '__main__':
    unittest.main()
    doctest.testmod()
