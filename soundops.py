from mysound import *


def soundadd(s1, s2: Sound):
    sound = Sound(max(s1.duration, s2.duration))

    sound.buffer = (list(map(lambda x, y: x + y,
                    s1.buffer[0:min(s1.nsamples, s2.nsamples)],
                    s2.buffer[0:min(s1.nsamples, s2.nsamples)]))
                    + s1.buffer[min(s1.nsamples, s2.nsamples)+1:
                                max(s1.nsamples, s2.nsamples)]
                    + s2.buffer[min(s1.nsamples, s2.nsamples)+1:
                                max(s1.nsamples, s2.nsamples)])
    return sound
